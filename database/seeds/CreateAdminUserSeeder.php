<?php
  
use Illuminate\Database\Seeder;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
  
class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Hardik Savani', 
            'Gender' => 'Male',
        	'email' => 'admin@gmail.com',
        	'password' => bcrypt('123456')
        ]);

        $user2 = User::create([
            'name' => 'Seori', 
            'Gender' => 'Female',
        	'email' => 'seori@gmail.com',
        	'password' => bcrypt('seori')
        ]);
  
        $role = Role::create(['name' => 'Admin']);
        $role2 = Role::create(['name' => 'User']);
   
        $permissions = Permission::pluck('id','id')->all();
        $permission1 = Permission::where('id', 1)->get();
        $permission2 = Permission::where('id', 5)->get();
        $permission3 = Permission::where('id', 9)->get();
  
        $role->syncPermissions($permissions);
        $role2->givePermissionTo($permission1);
        $role2->givePermissionTo($permission2);
        $role2->givePermissionTo($permission3);
   
        $user->assignRole([$role->id]);
        $user2->assignRole([$role2->id]);
    }
}